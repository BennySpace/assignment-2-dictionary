; lib.asm
; Ver. 1.4

section .text

; Syscall IDs section 
%define SYSCALL_EXIT 60
%define SYSCALL_WR 1

; Symbols section
%define SYMB_SPACE ` `
%define SYMB_TAB `\t`
%define SYMB_NL `\n`

; Flags section
%define FLAG_FALSE 0
%define FLAG_TRUE 1

%define STD_OUT 1
%define STD_ERR 2


; Принимает код возврата и завершает текущий процесс
global exit
exit:
	mov rax, SYSCALL_EXIT

	syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
global string_length
string_length:
	xor rax, rax

	.loop:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .loop

	.end:
		ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:
	push rdi
    push rsi
	call string_length
	mov rdx, rax
    pop rdi
    pop rsi
	mov rax, SYSCALL_WR

	syscall
	ret


; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
	mov rdi, SYMB_NL


; Принимает код символа и выводит его в stdout
global print_char
print_char:
	push rdi
	mov rax, SYSCALL_WR
	mov rdi, STD_OUT
	mov rdx, STD_OUT 
	mov rsi, rsp

	syscall
	pop rdi
	ret


; Выводит знаковое 8-байтовое число в десятичном формате 
global print_int
print_int:
	cmp rdi, 0
	jge print_uint
	push rdi
	mov rdi, '-'

	call print_char

	pop rdi
	neg rdi


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
global print_uint
print_uint:
	xor rdx, rdx
	mov rax, rdi
	mov r8, rsp
	dec rsp
	mov byte[rsp],dl
	mov r9, 10

	.loop: 
		xor rdx, rdx
		div r9
		add rdx, '0'

		dec rsp
		mov byte[rsp],dl
		test rax, rax
		ja .loop
		mov rdi,rsp
		push r8
		call print_string
		pop rsp

		ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
	xor rax, rax

	.loop:
		mov cl, byte [rdi+rax]
		mov dl, byte [rsi+rax]
		cmp cl, dl
		jne .not_equal
		test cl, cl
		je .is_equal
		inc rax
		jmp .loop

	.is_equal:
		mov rax, FLAG_TRUE

		ret

	.not_equal:
		mov rax, FLAG_FALSE
		
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
	xor rax, rax
	push 0     
	mov rdi, 0
	mov rdx, STD_OUT
	mov rsi, rsp

	syscall
	pop rax
	ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
global read_word
read_word:
	push r12
	push r13
	push r14
	mov r12, rdi
	mov r13, rsi
	mov r14, 0

	.parse:
		call read_char
		cmp rax, SYMB_SPACE
		je .parse
		cmp rax, SYMB_TAB
		je .parse
		cmp rax, SYMB_NL
		je .parse
		xor r14, r14

	.loop:
		test rax, rax
		je .done
		cmp rax, SYMB_SPACE
		je .done
		cmp rax, SYMB_TAB
		je .done
		cmp rax, SYMB_NL
		je .done
		cmp r13, r14
		je .incorrect
		mov byte[r12+r14], al
		inc r14
		call read_char
		jmp .loop

	.done:
		mov byte[r12+r14], 0
		mov rax, r12
		mov rdx, r14
		jmp .end

	.incorrect:
		xor rax, rax

	.end:
		pop r14
		pop r13
		pop r12
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
global parse_uint
parse_uint:
	xor rax, rax
	xor rcx, rcx

	.loop:
		mov r8b, byte[rdi+rcx]
		cmp r8b, '0'

		jl .done
		cmp r8b, '9'

		jg .done
		imul rax, 10
		sub r8b, '0'

		add rax, r8
		inc rcx
		jmp .loop

	.done:
		mov rdx, rcx
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
	mov	al, [rdi]
	cmp	al, '-'

	je .negative
	cmp	al, '+'

	jne .positive
	inc rdi

	.positive:
		jmp parse_uint

	.negative:
		inc rdi
		call parse_uint
		neg rax
		inc rdx

		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
global string_copy
string_copy:
	xor rax, rax

	.loop:
		cmp rax, rdx
		jge .null_rax
		mov r9b, byte[rdi+rax]
		mov byte[rsi+rax], r9b
		inc rax
		test r9b, r9b
		jz .done
		jmp .loop

	.null_rax:
		xor rax, rax

	.done:
		ret

; Выводит строку ошибки в стандартный поток ошибок (stderr)
; Принимает адрес строки ошибки
global print_error
print_error:
	push rdi
	call string_length
	pop rdi
	mov rdx, rax
	mov rax, STD_OUT
	mov rsi, rdi
	mov rdi, STD_ERR

	syscall
	ret

; Читает строку из входного потока и сохраняет её в буфер
; Принимает адрес буфера (rdi), в который будет сохранена строка, а также максимальную длину строки для чтения (rsi)
global read_line
read_line:
	push r12
	push r13
	push r14
	xor r14, r14        
	mov r12, rdi        
	mov r13, rsi     

	.buffer_empty:
		test r13, r13    
		jz .stop

	.read_symbols:
		call read_char    
		test rax, rax         
		jz .stop
		cmp al, SYMB_NL     
		je .stop
		mov byte[r12+r14], al   
		inc r14           
		cmp r14, r13        
		jae .max_size
		jmp .read_symbols
    
	.max_size:
		xor rax, rax       
		jmp .ret

	.stop:    
		mov rdx, r14        
		mov rax, r12   
				
	.ret:
		pop r14
		pop r13
		pop r12
		ret