; main.asm
; ver. 1.0

%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define INDEX_OFFSET 8
%define NEW_LINE_CHAR 0xA
%define BUFFER_SIZE 255

section .rodata
not_found_message: 
	db "Word not found!", 0

read_fail_message: 
	db "Word too long (must be less than 255 symbols)!", 0

section .bss
input_buffer: 
	resb BUFFER_SIZE


section .text
global _start
_start:
	mov rdi, input_buffer
	mov rsi, BUFFER_SIZE
	call read_line
	test rax, rax
	jz .read_fail

	push rdx
	mov rdi, input_buffer
	mov rsi, next_element
	call find_word
	pop rdx

	test rax, rax
	jz .not_found

    add rax, INDEX_OFFSET
    add rax, rdx
    inc rax

    mov rdi, rax
    push rsi
    mov rsi, 1
    call print_string
    pop rsi

	.exit:
        xor rdi, rdi
        call exit

	.not_found:
		mov rdi, not_found_message
		jmp .error

	.read_fail:
		mov rdi, read_fail_message

	.error:
		push rsi
        mov rsi, 2
        call print_string
        pop rsi
        jmp .exit