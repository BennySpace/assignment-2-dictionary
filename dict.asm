; dict.asm
; ver. 1.0

%include "lib.inc"
%define WORDS_OFFSET 8

section .text

global find_word
find_word:
	push r12
	push r13
	push r14

	mov r12, rdi
	
	.loop:
		mov rdi, r12
		mov r14, rsi
		add rsi, WORDS_OFFSET
		call string_equals
		
		test rax, rax
		jnz .found

		mov r13, [r14]
		test r13, r13
		jz .not_found

		mov rsi, r13
		jmp .loop

	.not_found:
		xor rax, rax
		jmp .end

	.found:
		mov rax, r14

	.end:
		pop r14
		pop r13
		pop r12
		ret