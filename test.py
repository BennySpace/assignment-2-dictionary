from subprocess import Popen, PIPE

test_data = [
    ("Ren", "Amamiya", "", ""),
    ("Souji", "", "Word not found!", ""),
    ("Makotoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo", "", "Word too long (must be less than 255 symbols)!", "")
]

def run_tests():
    print("-------------------")

    for i, (input_data, expected_output, expected_error, _) in enumerate(test_data):
        process = Popen(["./main"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate(input=input_data.encode())
        stdout = stdout.decode().strip()
        stderr = stderr.decode().strip()

        if stdout == expected_output and stderr == expected_error:
            result = "SUCCESSFUL"
        else:
            result = "FAILURE"

        print(f"Test {i + 1}: {result}")
        print(f"Input: {input_data}")
        print(f"Expected out: {expected_output}")
        print(f"Actual out: {stdout}")
        print(f"Expected error: {expected_error}")
        print(f"Actual error: {stderr}")
        print(f"Exit code: {process.returncode}")
        print("-------------------")

run_tests()
